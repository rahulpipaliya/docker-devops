FROM node:slim
WORKDIR /var/lib/jenkins/workspace/auth
COPY package.json /var/lib/jenkins/workspace/auth/
RUN npm install
COPY . /var/lib/jenkins/workspace/auth/
CMD node index.js
EXPOSE 1337

